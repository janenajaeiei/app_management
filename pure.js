const http = require('http');

const server = http.createServer((req,res)=>{
    console.log(req.url);
    if(req.url == '/a'){
        a(res);
    }else if(req.url == '/b'){
        b(res);
    }else{
        res.write('404');
        res.end();
    }
});

function a(res){
    res.write('I am A')
    res.end();
}

function b(res){
    res.write('I am B');
    res.end();
}


server.listen('3000');
