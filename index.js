const express = require('express');
const funct = require('./src/connect');

const server = express();
server.get('/app_checker/checker/:app_name',funct.GetAppLastVersion);
server.get('/app_checker/download',funct.DownloadLastVersion);
server.get('/app_checker/GetAllApp',funct.GetAllApplication);

server.listen('3101',cb=>{
    console.log('Server is started'); 
});

