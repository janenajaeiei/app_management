'use strict';
const sql = require('mssql');

const config = {
    user: 'admin_prod',
    password: 'PreciseI$#910',
    server: '192.168.66.22',
    database: 'Application',
}

async function GetAppLastVersion(req, res) {
    try {
        await sql.connect(config)
        const result = await sql.query `select top 1 
        name.id as app_id 
          ,name.name as app_name 
          ,version.id as version_id
          ,version.name_id
          ,version.version
          ,version.file_path
          ,version.file_name
          ,version.file_type
          ,version.create_date
        from name 
        left join version on name.id = version.name_id
        where name.name = ${req.params.app_name} order by create_date desc`
        res.send(result.recordsets[0][0])
    } catch (err) {
        console.log(err);
    }
    res.end();
    sql.close();
}

function DownloadLastVersion(req, res) {
    var file = '../application/pr_po/1.3.1/prpo_application.apk';
    res.download(file); // Set disposition and send it.
}

async function GetAllApplication(req,res){
    try{
        await sql.connect(config)
        const result = await sql.query `select * from name`
        res.send(result.recordsets[0])
    }catch(err){
        console.log(err);
    }
    res.end()
    sql.close()
}




module.exports = {
    GetAppLastVersion,
    DownloadLastVersion,
    GetAllApplication
}